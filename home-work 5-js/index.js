"use strict";

/*Теоретичні питання

1. Як можна сторити функцію та як ми можемо її викликати?
Фунцію можна створити 3 способами: function declaration, function expression та arrow function.

2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
Оператор return в JavaScript повертає значення та завершує виконання функціі.

3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
Параметри функціі - це те що ми вказуємо в круглих дужках при створенні функціі. Аргументи функціі - це значення які ми передаємо (вказуємо) при виклику функціі.

4. Як передати функцію аргументом в іншу функцію?
Передача функціі аргументом в іншу функцію - це callback. 
*/

// Практичні завдання
// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

const calcNumberDivision = (num1, num2) => num1 / num2;
console.log(calcNumberDivision(20, 2));

// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
// - Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
// - Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.

const calcSum = (a, b) => +a + +b;
const calcMinus = (a, b) => a - b;
const calcMultiply = (a, b) => a * b;
const calcDivision = (a, b) => a / b;


let numberOne;

while (!numberOne || isNaN(numberOne)) {
    numberOne = +prompt("Enter your first number");
}

let numberTwo;

while (!numberTwo || isNaN(numberTwo)) {
    numberTwo = +prompt("Enter your second number");
}

let operator = prompt("Choose mathematical operation +, -, /, *")
if (operator !== "+" && operator !== "-" && operator !== "/" && operator !== "*") {
    alert("It's not a mathematical operation");
}

const calcMathOperation = (a, b, operator) => {
    switch (operator) {
        case "+": {
            return calcSum(numberOne, numberTwo);
        }
        case "-": {
            return calcMinus(numberOne, numberTwo);
        }
        case "/": {
            return calcDivision(numberOne, numberTwo);
        }
        case "*": {
            return calcMultiply(numberOne, numberTwo);
        }
    }
}
console.log(calcMathOperation(numberOne, numberTwo, operator));