"use strict";
// Теоретичні питання

// 1. Опишіть своїми словами, що таке метод об'єкту?
// Метод обьекту - це функція в обьєкті.

// 2. Який тип даних може мати значення властивості об'єкта?
// Властивості об'єкта можуть бути рядком, числом, масивом, функцією.

// 3. Об'єкт це посилальний тип даних. Що означає це поняття?
// Це поняття означає, що при копіюванні обьєкта, він копіюється не за значенням, а за посиланням.

// Практичні завдання
// 1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.


const product = {
    name: "book",
    price: 200,
    discount: 50,
    calcPriseWithDiscount(discount) {
        this.discount = 50 / 100;
        let priceWithDiscount = this.price * [this.discount];
        return priceWithDiscount;
    },
};

console.log(product.calcPriseWithDiscount());

// 2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
// наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
// за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.

const showGreeting = (obj) => {

    return (`Hello my name is ${obj.name}, I'm ${obj.age} years old.`);
};

const name = prompt("Enter your name");
const age = prompt("Enter your age");

const user = {
    name,
    age,
};

alert(showGreeting(user));


