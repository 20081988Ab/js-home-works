"use strict";

/*   
1. Як можна оголосити змінну у Javascript?
Оголосити змінну у Javascript за допомогою let або const.

2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?
Рядок (string) - це один з 8 типів даних JS. Щоб створити рядок треба взяти значення змінноі в лапки. Також можна отримати рядок за допомогою toString(), або конструктора String().
Конкатенація також є одним із способів оримання рядка.

3. Як перевірити тип даних змінної в JavaScript?
Перевірити тип даних змінної в JavaScript можна за допомогою typeof.

4. Поясніть чому '1' + 1 = 11.
'1' + 1 = 11 із за того що один операнд є рядком, тобто відбувается конкатенація.
*/

// Практичні завдання
// 1. Оголосіть змінну і присвойте в неї число. Перевірте, чи ця змінна має тип "number" і виведіть результат в консоль.

const age = 18;
console.log(typeof age);

// 2. Оголосіть змінну name і присвойте їй ваше ім'я. Оголосіть змінну lastName і присвойте в неї Ваше прізвище.

const userName = "Anna";
const lastName = "Kasian";
console.log(`Мене звати ${userName} ${lastName}`);

// 3. Оголосіть змінну з числовим значенням і виведіть в консоль її значення всередині рядка.

const userAge = 18;
console.log(`Мене звати ${userName} ${lastName}, мені ${userAge} років.`);