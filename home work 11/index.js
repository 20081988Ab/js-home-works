
// Теоретичні питання

// 1. Що таке події в JavaScript і для чого вони використовуються?
// Подія – це сигнал від браузера про те, що щось сталося.

// 2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
// mousedown/mouseup
// mouseover/mouseout
// mousemove

// 3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
// Викликається при спробі відкриття контекстного меню, зазвичай, натисканням правої кнопки миші. 
// /Може викликатись і спеціальною клавішею клавіатури.


// Практичні завдання
//  1. Додати новий абзац по кліку на кнопку:
//   По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

const button = document.querySelector("#btn-click");
const section = document.querySelector("#content");
button.addEventListener("click", () => {
    const p = document.createElement('p');
    p.innerText = "New Paragraph";
    section.append(p);
})

//  2. Додати новий елемент форми із атрибутами:
//  Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//     По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
//  
const buttonCreateInput = document.createElement('button');
buttonCreateInput.id = "btn-input-create";
buttonCreateInput.innerText = "Сreate input"

const sectionContent = document.querySelector("#content");
sectionContent.append(buttonCreateInput);

buttonCreateInput.addEventListener("click", () => {
    const input = document.createElement("input");
    input.type = "text";
    input.name = "name";
    input.placeholder = "Enter your name";
    buttonCreateInput.after(input);
})
