/*Теоретичні питання
1. Що таке цикл в програмуванні?
Цикл - це можливість виконувати первний блок повторно.

2. Які види циклів є в JavaScript і які їх ключові слова?
В JavaScript є цикл for, while та do while.

3. Чим відрізняється цикл do while від while?
Цикл do while, на відміну від while, спочатку виконує тіло циклу один раз і тільки після цього перевіряє умову.
*/

// 1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом. Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом. Виведіть на екран всі числа від меншого до більшого за допомогою циклу for. 

let firstNumber;
let secondNumber;

do {
    firstNumber = +prompt("Enter your first number");
} while (!firstNumber);

do {
    secondNumber = +prompt("Enter your second number");
} while (!secondNumber);

if (firstNumber <= secondNumber) {
    for (let i = firstNumber; i <= secondNumber; i++) {
        console.log(i);
    }
} else {
    for (let i = secondNumber; i <= firstNumber; i++) {
        console.log(i)
    }
}


// 2. Напишіть програму, яка запитує в користувача число та перевіряє,
//  чи воно є парним числом. Якщо введене значення не є парним числом,
//  то запитуйте число доки користувач не введе правильне значення.

let number;
do {
    number = +prompt("Enter your number");
} while (number % 2 !== 0);

