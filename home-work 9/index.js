
// Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)

// DOM схожий на вихідний код HTML. DOM підтримує об'єктно орієнтоване 
// представлення веб-сторінки і дозволяє змінювати документ веб-сторінки за допомогою JavaScript.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerText не може містити HTML теги;
// innerHTML містить HTML теги;

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// document.getElementById() 
// document.getElementsByName() 
// document.getElementsByTagName()
// document.getElementsByClassName() 
// document.querySelector() 
// document.querySelectorAll() 

// Мабудь останні два методи більш зручні, бо простіше прописати що саме ти шукаєш в (). 

// 4. Яка різниця між nodeList та HTMLCollection?
// HTMLCollection завжди є живою колекцією, а NodeList є статичною колекцією. В NodeList можно працювати з forEach()

// Практичні завдання
//  1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
//  Використайте 2 способи для пошуку елементів. 
//  Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).

const elemCollection = document.querySelectorAll(".feature");
elemCollection.forEach(elem => {
    console.log(elem)
    elem.style.textAlign = "center"
})

console.log(elemCollection);

const elemCollection2 = document.getElementsByClassName("feature");
console.log(elemCollection2);



//  2. Змініть текст усіх елементів h2 на "Awesome feature".


const titleCollection = document.querySelectorAll("h2");
console.log(titleCollection);

titleCollection.forEach(elem => elem.innerText = "Awesome feature")


//  3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".

const featureTitleCollection = document.querySelectorAll(".feature-title");
featureTitleCollection.forEach(elem => elem.innerText += "!")
console.log(featureTitleCollection); 